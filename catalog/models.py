from django.db import models
from datetime import date
from django.contrib.auth.models import User
import uuid 

# Create your models here.

class Language(models.Model):
    """Model representing a Language (e.g. English, French, Japanese, etc.)"""
    name = models.CharField(max_length=200,
                            help_text="Enter the book's natural language (e.g. English, French, Japanese etc.)")

    def __str__(self):
        """String for representing the Model object (in Admin site etc.)"""
        return self.name

class Genre(models.Model):
    """
    Modelo que representa un género literario (p. ej. ciencia ficción, poesía, etc.).
    Un campo para el id de la tupla o instancia es añadido automáticamente de tipo AutoField
    """
    name = models.CharField(max_length=200, help_text="Ingrese el nombre del género (p. ej. Ciencia Ficción, Poesía Francesa etc.)")

    def __str__(self):
        """
        Cadena que representa a la instancia particular del modelo (p. ej. en el sitio de Administración)
        """
        return self.name

from django.urls import reverse #Used to generate URLs by reversing the URL patterns

class Book(models.Model):
    """
    Modelo que representa un libro (pero no un Ejemplar específico).
    """

    title = models.CharField(max_length=200)

    author = models.ForeignKey('Author', on_delete=models.SET_NULL, null=True)
    # ForeignKey, ya que un libro tiene un solo autor, pero el mismo autor puede haber escrito muchos libros.
    # 'Author' es un string, en vez de un objeto, porque la clase Author aún no ha sido declarada.

    summary = models.TextField(max_length=1000, help_text="Ingrese una breve descripción del libro")

    isbn = models.CharField('ISBN',max_length=13, help_text='13 Caracteres <a href="https://www.isbn-international.org/content/what-isbn">ISBN number</a>')

    genre = models.ManyToManyField(Genre, help_text="Seleccione un genero para este libro")
    # ManyToManyField, porque un género puede contener muchos libros y un libro puede cubrir varios géneros.
    # La clase Genre ya ha sido definida, entonces podemos especificar el objeto arriba.
    language = models.ForeignKey('Language', on_delete=models.SET_NULL, null=True)

    class Meta:
        ordering = ["isbn"]
        permissions = (
                       ("can_update_book_data", "Update Book Data"),
                      )

    def __str__(self):
        """
        String que representa al objeto Book
        """
        return self.title


    def get_absolute_url(self):
        """
        Devuelve el URL a una instancia particular de Book
        """
        return reverse('book-detail', args=[str(self.id)])

    ''' As book-genre is a many-to-many rel, we'll generat the book genre as
        the first 3 genres joined by ,
    '''

    def display_genre(self):
        """
        Creates a string for the Genre. This is required to display genre in Admin.
        display_genre.short_description will be used as the column header in the
        Admin interface ...
        """
        return ', '.join([ genre.name for genre in self.genre.all()[:3] ])

    display_genre.short_description = 'Genre'

class BookInstance(models.Model):
    '''
    Modelo que representa una copia específica de un libro (i.e. que puede ser prestado por la biblioteca).
    The UUIDField data type generates unique string identifiers of this kind:
              9c02aacf-a391-401c-90f4-9844fb0194c2

    From the ForeignKey definition below, we can see that there is a  Book(1) - (N)BookInstance
    relationtion (we did not specify anything in parent entity). This allow us to reach all the
    BookInstance instances for a particular Book using book.bookinstance_set.all. The same applies
    for Author (where we do not specify anithing) and Books (where we set up another ForeignKey)
    '''
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, help_text="ID único para este libro particular en toda la biblioteca")
    book = models.ForeignKey('Book', on_delete=models.SET_NULL, null=True)
    borrower = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    imprint = models.CharField(max_length=200)
    due_back = models.DateField(null=True, blank=True)

    LOAN_STATUS = (
        ('m', 'Maintenance'),
        ('o', 'On loan'),
        ('a', 'Available'),
        ('r', 'Reserved'),
    )

    '''
        get_status_display is a method that can be used in the corresponding
        detail template to access de status description (FROM LOAN_STATUS)
        instead of status code), for better usability
        (see templates/catalog/book_detail.html)
    '''

    status = models.CharField(max_length=1, choices=LOAN_STATUS, blank=True, default='m', help_text='Disponibilidad del libro')

    class Meta:
        ordering = ["due_back"]
        permissions = (
                       ("can_mark_returned", "Set book as returned"),
                       ("can_list_all_borrowed", "List all borrowed books")
                      )


    def __str__(self):
        """
        String para representar el Objeto del Modelo
        """
        return '%s [%s - %s]' % (self.id,self.book.title, self.book.author)

    @property
    def is_overdue(self):
        if self.due_back and date.today() > self.due_back:
            return True
        return False

    @property
    def is_available(self):
        if self.status == 'a':
            return True
        return False
    

class Author(models.Model):

    """
    Modelo que representa un autor
    """
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    date_of_birth = models.DateField(null=True, blank=True)
    date_of_death = models.DateField('Died', null=True, blank=True)

    def get_absolute_url(self):
        """
        Retorna la url para acceder a una instancia particular de un autor.
        example : '/catalog/author/1' 
        """
        return reverse('author-detail', args=[str(self.id)])

    class Meta:
        permissions = (
                       ("can_update_author_data", "Update Author Data"),
                      )

    def __str__(self):
        """
        String para representar el Objeto Modelo
        """
        return '%s, %s' % (self.last_name, self.first_name)


