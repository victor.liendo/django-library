from django.conf.urls import url
from . import views
from django.urls import include,path
'''
urlpatterns = [
   path('', views.index, name='index'),
]
'''

''' ^$ is a regular expression for an empty string,
thus, when a request of the type 
      http://..../ 
      http://..../catalog/ or

index function in views.py will ve invoked 
 
'''
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^books/$', views.BookListView.as_view(), name='books'),
    url(r'^books-as-json/$', views.books_as_json, name='books_as_json'),
    url(r'^book/(?P<pk>\d+)$', views.BookDetailView.as_view(), name='book-detail'),
    url(r'^authors/$', views.AuthorListView.as_view(), name='authors'),
    url(r'^author/(?P<pk>\d+)$', views.AuthorDetailView.as_view(), name='author-detail'),
]
urlpatterns += [
    url(r'^my-loaned-books/$', views.LoanedBooksByUserListView.as_view(), name='my-loaned'),
]
urlpatterns += [
    url(r'^all-loaned-books/$', views.LoanedBooksListView.as_view(), name='all-loaned'),
]
urlpatterns += [
    url(r'^book/(?P<pk>[-\w]+)/renew/$', views.renew_book_librarian, name='renew-book-librarian'),
    url(r'^book/(?P<pk>[-\w]+)/return/$', views.return_book_librarian, name='return-book-librarian'),
]

urlpatterns += [
    url(r'^author/create/$', views.AuthorCreate.as_view(), name='author_create'),
    url(r'^author/(?P<pk>\d+)/update/$', views.AuthorUpdate.as_view(), name='author_update'),
    url(r'^author/(?P<pk>\d+)/delete/$', views.AuthorDelete.as_view(), name='author_delete'),
    url(r'^book/create/$', views.BookCreate.as_view(), name='book_create'),
    url(r'^book/(?P<pk>\d+)/update/$', views.BookUpdate.as_view(), name='book_update'),
]