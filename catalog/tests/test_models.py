from django.test import TestCase
import datetime
from django.utils import timezone
from datetime import date

# Create your tests here.

from catalog.models import Language, Genre, Author, Book, BookInstance 


def create_book():
        #Set up non-modified objects used by all test methods
        language=Language.objects.create(name='English')
        '''language.save()'''
        author=Author.objects.create(first_name='Foo', last_name='Bar')
        '''author.save()'''
        book=Book.objects.create(
                            title='Matrix Reloaded',
                            summary='What is the actual reality, that in the Matrix or the one out of it?',
                            isbn='ISBN123456789',
                            author=author,
                            language=language
                            )
        ''' Required for a M-to-M relationship '''
        '''book.save()'''
        genre=Genre.objects.create(name='SCIFI')
        '''genre.save()'''
        book.genre.add(genre)
        genre=Genre.objects.create(name='THRILLER')
        book.genre.add(genre)
        '''book.save()'''
        
        return book
        
def create_book_instance(days,status):

    book=create_book()
    '''time = timezone.now() + datetime.timedelta(days=days)'''
    due_back = date.today() + datetime.timedelta(days=days)
    binstance=BookInstance.objects.create(
                                    book=book,
                                    borrower=None,
                                    imprint='Xvvzb8100XtFWQAZ',
                                    due_back=due_back,
                                    status=status
                                    )
    return binstance


class LanguageModelTest(TestCase):

    ''' for the setUpTestData(cls) method tyo be executed, there must be at least
        1 test method defined '''

    @classmethod
    def setUpTestData(cls):
        #Set up non-modified objects used by all test methods
        Language.objects.create(name='English')

    def test_name_label(self):
        language=Language.objects.get(id=1)
        ''' verbose_name returns the field label '''
        field_label = language._meta.get_field('name').verbose_name
        self.assertEquals(field_label,'name')
    
    def test_object_name_is_name(self):
        language=Language.objects.get(id=1)
        expected_object_name = '%s' % (language.name)
        '''testing the str method'''
        self.assertEquals(expected_object_name,str(language))

class GenreModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        #Set up non-modified objects used by all test methods
        Genre.objects.create(name='Thriller')

    def test_name_label(self):
        genre=Genre.objects.get(id=1)
        ''' verbose_name returns the field label '''
        field_label = genre._meta.get_field('name').verbose_name
        self.assertEquals(field_label,'name')
    
    def test_object_name_is_name(self):
        genre=Genre.objects.get(id=1)
        expected_object_name = '%s' % (genre.name)
        '''testing the str method'''
        self.assertEquals(expected_object_name,str(genre))

class AuthorModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        #Set up non-modified objects used by all test methods
        Author.objects.create(first_name='Thomas', last_name='Parr')

    def test_first_name_label(self):
        author=Author.objects.get(id=1)
        ''' verbose_name returns the field label '''
        field_label = author._meta.get_field('first_name').verbose_name
        self.assertEquals(field_label,'first name')

    def test_last_name_label(self):
        author=Author.objects.get(id=1)
        field_label = author._meta.get_field('last_name').verbose_name
        self.assertEquals(field_label,'last name')

    
    def test_date_of_birth_label(self):
        author=Author.objects.get(id=1)
        field_label = author._meta.get_field('date_of_birth').verbose_name
        self.assertEquals(field_label,'date of birth')

    def test_date_of_death_label(self):
        author=Author.objects.get(id=1)
        field_label = author._meta.get_field('date_of_death').verbose_name
        self.assertEquals(field_label,'died')

    def test_first_name_max_length(self):
        author=Author.objects.get(id=1)
        max_length = author._meta.get_field('first_name').max_length
        self.assertEquals(max_length,100)

    def test_last_name_max_length(self):
        author=Author.objects.get(id=1)
        max_length = author._meta.get_field('last_name').max_length
        self.assertEquals(max_length,100)
    
    def test_object_name_is_last_name_comma_first_name(self):
        author=Author.objects.get(id=1)
        expected_object_name = '%s, %s' % (author.last_name, author.first_name)
        '''testing the str method'''
        self.assertEquals(expected_object_name,str(author))

    def test_get_absolute_url(self):
        author=Author.objects.get(id=1)
        #This will also fail if the urlconf is not defined.
        self.assertEquals(author.get_absolute_url(),'/catalog/author/2')

class BookModelTest(TestCase):

    def test_title_label(self):
        book=create_book()
        ''' verbose_name returns the field label '''
        field_label = book._meta.get_field('title').verbose_name
        self.assertEquals(field_label,'title')

    def test_author_label(self):
        book=create_book()
        ''' verbose_name returns the field label '''
        field_label = book._meta.get_field('author').verbose_name
        self.assertEquals(field_label,'author')

    def test_summary_label(self):
        book=create_book()
        ''' verbose_name returns the field label '''
        field_label = book._meta.get_field('summary').verbose_name
        self.assertEquals(field_label,'summary')
    
    def test_isbn_label(self):
        book=create_book()
        ''' verbose_name returns the field label '''
        field_label = book._meta.get_field('isbn').verbose_name
        self.assertEquals(field_label,'ISBN')
    
    def test_title_max_length(self):
        book=create_book()
        max_length = book._meta.get_field('title').max_length
        self.assertEquals(max_length,200)

    def test_summary_max_length(self):
        book=create_book()
        max_length = book._meta.get_field('summary').max_length
        self.assertEquals(max_length,1000)

    def test_isbn_max_length(self):
        book=create_book()
        max_length = book._meta.get_field('isbn').max_length
        self.assertEquals(max_length,13)

    def test_get_absolute_url(self):
        #This will also fail if the urlconf is not defined.
        book=create_book()
        self.assertEquals(book.get_absolute_url(),'/catalog/book/1')

    def test_genre_count(self):
        book=create_book()
        genres=[genre.name for genre in book.genre.all()]
        self.assertEquals(len(genres),2)

    def test_display_genre(self):
        book=create_book()
        expected_genre_name = 'SCIFI, THRILLER'
        '''testing the display_genre method'''
        self.assertEquals(expected_genre_name,book.display_genre())
    
    def test_object_name_is_name(self):
        book=create_book()
        expected_object_name = '%s' % (book.title)
        '''testing the str method'''
        self.assertEquals(expected_object_name,str(book))

class BookInstanceModelTest(TestCase):
    
    
    def test_borrower_label(self):

        '''
        This returns the first element in a QuerySet
        binstance = BookInstance.objects.first()
        '''
        binstance = create_book_instance(10,'a')
        field_label = binstance._meta.get_field('borrower').verbose_name
        self.assertEquals(field_label,'borrower')

    def test_imprint_label(self):
        binstance =  binstance = create_book_instance(10,'a')
        field_label = binstance._meta.get_field('imprint').verbose_name
        self.assertEquals(field_label,'imprint')
    
    def test_due_back_label(self):
        binstance = create_book_instance(10,'a')
        field_label = binstance._meta.get_field('due_back').verbose_name
        self.assertEquals(field_label,'due back')
    
    def test_borrower_label(self):
        binstance = create_book_instance(10,'a')
        field_label = binstance._meta.get_field('status').verbose_name
        self.assertEquals(field_label,'status')

    def test_imprint_max_length(self):
        binstance = create_book_instance(10,'a')
        max_length = binstance._meta.get_field('imprint').max_length
        self.assertEquals(max_length,200)

    def test_is_overdue(self):
        binstance = create_book_instance(-1,'a')
        self.assertTrue(binstance.is_overdue)

    def test_is_not_overdue(self):
        binstance = create_book_instance(+1,'a')
        self.assertFalse(binstance.is_overdue)

    def test_is_available(self):
        binstance = create_book_instance(+1,'a')
        self.assertTrue(binstance.is_available)

    def test_is_not_available_m(self):
        binstance = create_book_instance(+1,'m')
        self.assertFalse(binstance.is_available)

    def test_is_not_available_o(self):
        binstance = create_book_instance(+1,'o')
        self.assertFalse(binstance.is_available)

    def test_is_not_available_r(self):
        binstance = create_book_instance(+1,'r')
        self.assertFalse(binstance.is_available)  
        
        
    