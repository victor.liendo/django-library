from django.shortcuts import render
from django.http import HttpResponse
from django.views import generic
from django.contrib.auth.decorators import permission_required
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from .models import Author

""" Nos permite que el acceso a una vista GENERICA este restringido a 
    usuarios loggeados. Las funciones de vista que hereden de esta clase
    tendrán esa característica """
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin

''' Para el manejo de la vista que renderiza el formulñario '''
from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
import datetime

from .forms import RenewBookForm

''' for JSON response example '''
from django.http import JsonResponse


'''
def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")
'''

from .models import Book, Author, BookInstance, Genre

def index(request):
    """
    Función vista para la página inicio del sitio.
    """
    # Genera contadores de algunos de los objetos principales
    num_genres=Genre.objects.all().count()
    num_books=Book.objects.all().count()
    num_instances=BookInstance.objects.all().count()
    # Libros disponibles (status = 'a')
    num_instances_available=BookInstance.objects.filter(status__exact='a').count()
    num_authors=Author.objects.count()  # El 'all()' esta implícito por defecto.

    # Numero de visitas a esta view, como está contado en la variable de sesión.
    num_visits = request.session.get('num_visits', 0)
    request.session['num_visits'] = num_visits + 1


    # Renderiza la plantilla HTML index.html con los datos en la variable contexto
    return render(
        request,
        'index.html',
        context={   
                    'num_visits': num_visits,
                    'num_genres':num_genres,
                    'num_books':num_books,
                    'num_instances':num_instances,
                    'num_instances_available':num_instances_available,
                    'num_authors':num_authors
                },
    )

''' SORT OF REST endpoint '''
def books_as_json(request):
    #users = Book.objects.all().values('first_name', 'last_name')  # or simply .values() to get all fields
    books = Book.objects.all().values()
    book_list = list(books)  # important: convert the QuerySet to a list object
    return JsonResponse(book_list, safe=False)


''' Generic views ...
    For generic views, templates must be placed at /[app]/templates/[app] '''

class BookListView(generic.ListView):
    model = Book
    paginate_by = 5

class BookDetailView(generic.DetailView):
    model = Book

class AuthorListView(generic.ListView):
    model = Author
    paginate_by = 5

class AuthorDetailView(generic.DetailView):
    model = Author


class LoanedBooksByUserListView(LoginRequiredMixin,generic.ListView):
    """
    Generic class-based view listing books on loan to current user.
    """
    model = BookInstance
    template_name ='catalog/bookinstance_list_borrowed_user.html'
    paginate_by = 10

    """ We want to retrieve only those instances which has been borrowed
        (status = o), to the user in session """
        
    def get_queryset(self):
        return BookInstance.objects.filter(borrower=self.request.user).filter(status__exact='o').order_by('due_back')



class LoanedBooksListView(LoginRequiredMixin,generic.ListView):
    """
    Generic class-based view listing books on loan by all users user.
    """
    model = BookInstance
    permission_required = 'catalog.can_mark_returned'
    template_name ='catalog/bookinstance_list_borrowed.html'
    paginate_by = 10

    def get_queryset(self):
        return BookInstance.objects.filter(status__exact='o').order_by('due_back')

''' En una función típica esta es la forma de controlar el acceso a ella por permisos '''
@permission_required('catalog.can_mark_returned')
def renew_book_librarian(request, pk):
    """
    View function for renewing a specific BookInstance by librarian
    """
    book_inst=get_object_or_404(BookInstance, pk = pk)

    # If this is a POST request then process the Form data
    if request.method == 'POST':

        # Create a form instance and populate it with data from the request (binding):
        form = RenewBookForm(request.POST)

        # Check if the form is valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required (here we just write it to the model due_back field)
            book_inst.due_back = form.cleaned_data['renewal_date']
            book_inst.save()

            # redirect to a new URL:
            return HttpResponseRedirect(reverse('all-loaned') )

    # If this is a GET (or any other method) create the default form.
    else:
        proposed_renewal_date = datetime.date.today() + datetime.timedelta(weeks=3)
        form = RenewBookForm(initial={'renewal_date': proposed_renewal_date,})

    return render(request, 'catalog/book_renew_librarian.html', {'form': form, 'bookinst':book_inst})


@permission_required('catalog.can_mark_returned')
def return_book_librarian(request, pk):
    """
    View function for renewing a specific BookInstance by librarian
    """
    book_inst=get_object_or_404(BookInstance, pk = pk)

    if request.method == 'POST':
        book_inst.due_back = None
        book_inst.status = 'a'
        book_inst.save()
        return HttpResponseRedirect(reverse('all-loaned'))

    else:
        context = {'bookinst': book_inst}
        return render(request, 'catalog/book_return_librarian.html', context)


''' Vistas Genéricas de Formularios '''

''' En una vista basada en clases esta es la forma controlar el acceso a ella por permisos '''
class AuthorCreate(PermissionRequiredMixin,CreateView):
    permission_required = 'catalog.can_update_author_data'
    model = Author
    fields = '__all__'
    initial={'date_of_death':'05/01/2018',}


class AuthorUpdate(PermissionRequiredMixin,UpdateView):
    permission_required = 'catalog.can_update_author_data'
    model = Author
    fields = ['first_name','last_name','date_of_birth','date_of_death']

class AuthorDelete(PermissionRequiredMixin, DeleteView):
    permission_required = 'catalog.can_update_author_data'
    model = Author
    success_url = reverse_lazy('authors')

class BookCreate(PermissionRequiredMixin,CreateView):
    permission_required = 'catalog.can_update_book_data'
    model = Book
    fields = '__all__'
    initial={'title':'Your book title here ...',}

class BookUpdate(PermissionRequiredMixin,UpdateView):
    permission_required = 'catalog.can_update_book_data'
    model = Book
    fields = '__all__'
