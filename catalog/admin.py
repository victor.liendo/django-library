from django.contrib import admin

# Register your models here.

from .models import Author, Genre, Book, BookInstance, Language



''' Basic approach to register models in the admin site '''

admin.site.register(Language)
#admin.site.register(Author)
admin.site.register(Genre)
#admin.site.register(Book)
#admin.site.register(BookInstance)
#admin.site.register(Author, AuthorAdmin)

''' Lets use a more advanced approach:
    0) Change the admin site text header
    1) Specify wich fields we want to show for each model
    2) Define some filters for lists
    3) Change the way how fields or attributes are shown
    4) Show child instances for a parent one
'''

admin.site.site_header = 'Local Library Admin Site'

class BooksInstanceInline(admin.TabularInline):
    model = BookInstance
    ''' to avoid showing more lines than the number of instances
        that exists for a particular book  
    '''
    extra = 0

# Register the Admin classes using the decorator

@admin.register(Author)
class AuthorAdmin(admin.ModelAdmin):
    list_display = ('last_name', 'first_name', 'date_of_birth', 'date_of_death')
    fields = ['first_name', 'last_name', ('date_of_birth', 'date_of_death')]


@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    ''' display_genre is a function that has been defined for the Book model '''
    list_display = ('title', 'author', 'display_genre')
    list_filter = ('author','genre')

    ''' Allows to show instances in the detail interface for a particular Book
        , using what is definde in de BooksInstanceInline class '''
    inlines = [BooksInstanceInline]


@admin.register(BookInstance)
class BookInstanceAdmin(admin.ModelAdmin):
    list_filter = ('status', 'due_back')
    list_display = ('id', 'book', 'status', 'borrower', 'due_back')
    ''' defining sections to group fields. None for a section without a title,
        insted of Main Info, or Availability
    '''

    ''' for a book instance detail '''
    fieldsets = (
        ('Main Info', {
            'fields': ('book', 'imprint', 'id')
        }),
        ('Availability', {
            'fields': ('status', 'borrower', 'due_back')
        }),
    )




