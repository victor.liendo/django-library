# django-library

This projects implements a simple/basic  library management system where people can browse a catalog of author, books, copies and authorized users can perform renews, returns, and some other basic information like new authors and books.

This project was assembled by following the tutorial available at https://developer.mozilla.org/es/docs/Learn/Server-side/Django/Tutorial_local_library_website. So 90% percent of the ideas and code are not of my own, just added 2 o 3 features, because the purpose was mainly to get involved in the process of developing apps using the Django Framework
