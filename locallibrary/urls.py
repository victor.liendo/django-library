"""locallibrary URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include,path
from django.views.generic import RedirectView
# Use static() to add url mapping to serve static files during development (only)
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url


urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('catalog/', include('catalog.urls')),
    path('', RedirectView.as_view(url='/catalog/', permanent=True)),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

''' As the django default account app is not one created by us, it does not have a folder under
    the project directory (an thus, it does not have its own urls.py file). Hence, we must
    configure its urls here at the project level (locallibrary/urls.py) '''

urlpatterns += [
    url(r'^accounts/login/$', include('django.contrib.auth.urls'),  name=['login']),
    url(r'^accounts/logout/$', include('django.contrib.auth.urls'), name=['logout']),
    url(r'^accounts/password_change/$', include('django.contrib.auth.urls'), name=['password_change']),
    url(r'^accounts/password_change/done/$', include('django.contrib.auth.urls'), name=['password_change_done']),
    url(r'^accounts/password_reset/$', include('django.contrib.auth.urls'), name=['password_reset']),
    url(r'^accounts/password_reset/done/$', include('django.contrib.auth.urls'), name=['password_reset_done']),
    url(r'^accounts/reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', include('django.contrib.auth.urls'), name=['password_reset_confirm']),
    url(r'^accounts/reset/done/$', include('django.contrib.auth.urls'), name=['password_reset_complete'])
]


