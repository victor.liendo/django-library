0) TIPS
-------

   La carpeta templates es para las palntillas para la aplicación por defecto para manejo de usuarios ...
   esa app no es creada con el proyecto sino que ya existe ...

1) CORRECCIONES AL TUTORIAL
---------------------------

a) En la sección "Tutorial Django Parte 2: Creación del esqueleto del sitio web", 

	En el bloque 
	urlpatterns = [
    	path('admin/', admin.site.urls),
    	path('catalog/', include('catalog.urls')),
    	path('/', RedirectView.as_view(url='/catalog/', permanent=True)),
	] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


	La última línea de la lista debe ser 

	 path('', RedirectView.as_view(url='/catalog/', permanent=True)),


b) Antes de probar si la aplicación básica funciona, justo antes de corres las migraciones
	- Se sugiere incluir una vista básica en catalog/views.py

		from django.shortcuts import render
		from django.http import HttpResponse


		def index(request):
    			return HttpResponse("Hello, world. You're at the catalog index.")
  
	- Incluir esa vista en catalog/urls.py

		from django.conf.urls import url

		from . import views
		from django.urls import include,path

		urlpatterns = [
   			path('', views.index, name='index'),
		]	

c) En la sección autenticación, las urls de aplicación por defecto para manejo de cuentas de
usuario deben ser incluidas en locallibrary/urls.py, de la siguiente forma

urlpatterns += [
    url(r'^accounts/login/$', include('django.contrib.auth.urls'),  name=['login']),
    ...
] 
